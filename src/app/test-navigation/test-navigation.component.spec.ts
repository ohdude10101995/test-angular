import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestNavigationComponent } from './test-navigation.component';

describe('TestNavigationComponent', () => {
  let component: TestNavigationComponent;
  let fixture: ComponentFixture<TestNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
