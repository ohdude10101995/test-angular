import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestNavigationComponent} from './test-navigation/test-navigation.component';
import {HeroesComponent} from './heroes/heroes.component';
const routes: Routes = [
  { path : "home", component :HeroesComponent},
  { path: 'test-navigation', component: TestNavigationComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' }, // redirect to `first-component`

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [TestNavigationComponent];